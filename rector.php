<?php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use Rector\Set\ValueObject\LevelSetList;

return static function (RectorConfig $rectorConfig): void {
    $rectorConfig->paths([
        '/codebase/src', '/codebase/tests'
    ]);

    /** Documentation with Rector rules available here: https://github.com/rectorphp/rector/blob/main/docs/rector_rules_overview.md */
    //$rectorConfig->rule(InlineConstructorDefaultToPropertyRector::class);

    $rectorConfig->sets([
        LevelSetList::UP_TO_PHP_81,
    ]);
};
