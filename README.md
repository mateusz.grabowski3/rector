# Rector

Simple docker container to run upgrades and refactoring for local projects.

Local environment can be run by executing the following commands:
```shell
docker build -t rector .
```

To install dependencies:
```shell
docker run -it --rm --volume $PWD:/var/www/html rector composer install
```

### To run PHP upgrade to PHP 8.1

To run react you need to change ```/path/to/your/project``` to point absolute path to the project which you want to upgrade. 

```shell
docker run -it --rm --volume /path/to/your/project:/codebase --volume $PWD:/var/www/html rector vendor/bin/rector process --config=/var/www/html/rector.php
```
This command will process all files in ```/src``` and ```/tests``` directory of your project.
Rector configuration can be found in ```recotr.php``` file.

### To run doctrine/orm upgrade. 
#### It will use attributes instead of annotations for your Doctrine models.

```shell
docker run -it --rm --volume /path/to/your/project:/codebase --volume $PWD:/var/www/html rector vendor/bin/rector process --config=/var/www/html/rector-orm.php
```

To run react you need to change ```/path/to/your/models``` to point absolute path to the models which you want to upgrade.

You can customize rector rules, e.g.:
```php
 $rectorConfig->rule(InlineConstructorDefaultToPropertyRector::class);
 ```
Documentation with all rules is available here:
https://github.com/rectorphp/rector/blob/main/docs/rector_rules_overview.md
