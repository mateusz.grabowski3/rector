FROM php:8.1-cli

RUN apt update && apt install -y libzip-dev libsodium-dev && docker-php-ext-install sodium zip

COPY --from=composer:2 /usr/bin/composer /usr/local/bin/composer

COPY composer.json composer.json
COPY composer.lock composer.lock

RUN composer install \
    --no-plugins \
    --no-scripts \
    --prefer-dist

WORKDIR /var/www/html